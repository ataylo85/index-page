app.get('/menu', function(req, res){
    fs.readdir("public", (err, files) => {
        let menu = "<h1>My pages</h1><ul>";

        files.forEach(file => {
          let link = `<li><a href = '/${file}'>${file}</a></li>`
          menu += link
        });
        menu += "</ul>"
        res.send(menu); 
      });
})
